﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class PassengersController(
    IService<Passenger> personService,
    IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var passengers = await personService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Passenger>, IReadOnlyList<PassengerDataTransferObject>>(passengers);
    }
    
    [HttpGet("/flights/{flightId:long}/passengers")]
    [ProducesResponseType(typeof(IEnumerable<PassengerDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> GetPassengersByFlight(long flightId)
    {
        var passengers = await personService.FilterAsync(p => p.Flights.Any(x => x.Id == flightId));
        return !passengers.Succeeded ?
            NotFound($"No passengers were found for the flight {flightId}") :
            MapResultToDataTransferObject<IReadOnlyList<Passenger>, IReadOnlyList<PassengerDataTransferObject>>(passengers);
    }
    
    [HttpPost("/flights/{flightId:long}/passenger/{passengerId}")]
    [ProducesResponseType(typeof(PassengerDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> AddPassengerToFlight(
        [FromServices] IService<PassengerFlight> passengerFlightService,
        [FromServices] IService<Flight> flightService,
        long flightId,
        string passengerId)
    {
        //TODO: Check if already added
        
        //TODO: Check if doesn't exists
        var passengerResult = await personService.FilterAsync(p => p.Id == passengerId);
        var passenger = passengerResult.Result.First();

        //TODO: Check if doesn't exists
        var flightResult = await flightService.FilterAsync(f => f.Id == flightId);
        var flight = flightResult.Result.First();
        
        var passengerFlight = new PassengerFlight{FlightId = flight.Id, PassengerId = passenger.Id};
        var passengerFlightResult = await passengerFlightService.AddToAsync(passengerFlight);
        
        return !passengerFlightResult.Succeeded ?
            NotFound($"No passengers were found for the flight {flightId}") : //TODO: Improve message
            MapResultToDataTransferObject<PassengerFlight, PassengerFlightDataTransferObject>(passengerFlightResult);//TODO: Improve successful response
    }
}
